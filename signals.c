#include <errno.h>
#include <stdlib.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <signal.h>

#define BUFSIZE 512
#define ALARM_T 1

#define _str(x) #x
#define str(x) _str(x)

#define $(code...)                                                  \
    do {                                                            \
        errno = 0;                                                  \
        code;                                                       \
        if(errno != 0) {                                            \
            perror(#code " at " str(__LINE__));                     \
            exit(EXIT_FAILURE);                                     \
        }                                                           \
    } while(0)

#define TEST(cond, msg)                                             \
    do {                                                            \
        if(!(cond)) {                                               \
            fprintf(stderr, msg "\n");                              \
            fflush(stderr);                                         \
            exit(EXIT_FAILURE);                                     \
        }                                                           \
    } while(0)

void child(pid_t ppid, char const path[]);
void parent(pid_t cpid);

int  bit     = 0;
char byte    = 0;

int  bufidx  = 0;
char buf[BUFSIZE] = {};

int  incoming = 0;
void handlebit(int sig)
{
    incoming = 1;
    switch(sig) {
        case SIGUSR1:
            byte = (byte << 1) | 0;
            break;
        case SIGUSR2:
            byte = (byte << 1) | 1;
            break;
    }
    bit++;
}

void dummy(int sig) { }

void handlechld(int sig)
{
    int status = 0;
    $(waitpid(-1, &status, WNOHANG | WUNTRACED | WCONTINUED));

    if(!(WIFCONTINUED(status) || WIFSTOPPED(status))) {
        fprintf(stderr, "Child died\n");
        exit(EXIT_FAILURE);
    }
}

void handlestop(int sig)
{
    write(STDOUT_FILENO, buf, bufidx);
    exit(EXIT_SUCCESS);
}

int  ready = 0;
void handlecont(int sig)
{
    ready = 1;
}

void init_handlers()
{
    struct sigaction act = {};
    sigset_t set;

    act.sa_handler = handlebit;
    sigaction(SIGUSR1, &act, NULL);
    sigaction(SIGUSR2, &act, NULL);

    act.sa_handler = handlechld;
    sigaction(SIGCHLD, &act, NULL);

    act.sa_handler = dummy;
    sigaction(SIGALRM, &act, NULL);

    sigfillset(&set);
    act.sa_handler = handlestop;
    act.sa_mask = set;              //< SIGCHLD protection during stop handling
    sigaction(SIGIO, &act, NULL);

    sigemptyset(&set);
    sigaddset(&set, SIGALRM);
    sigprocmask(SIG_BLOCK, &set, NULL);
}

int main(int argc, char* const argv[])
{
    TEST(argc == 2, "Wrong amount of arguments");

    $(init_handlers());

    int ppid = getpid();
    int cpid = -1;
    switch(cpid = fork()) {
        case -1: {
            perror("fork");
            exit(EXIT_FAILURE);
        }
        case 0: {
            child(ppid, argv[1]);
            break;
        }
        default: {
            parent(cpid);
            break;
        }
    }

    return 0;
}

void waithandler(pid_t ppid)
{
    sigset_t set;
    sigemptyset(&set);

    while(!ready) {
        alarm(ALARM_T);         //< Race condition
        sigsuspend(&set);
        alarm(0);

        TEST(ppid == getppid(), "Parent connection lost");
    }
    ready = 0;
}

void child(pid_t ppid, char const path[])
{
    struct sigaction act = { .sa_handler = handlecont };
    sigaction(SIGIO, &act, NULL);

    int src_fd = -1;
    $(src_fd = open(path, O_RDONLY));

    int it_read = 0;
    do {
        $(it_read = read(src_fd, buf, BUFSIZE));

        for(bufidx = 0; bufidx < it_read; ++bufidx) {
            for(int i = 7; i >= 0; --i) {
                bit = (1 << i) & (buf[bufidx]);
                int sig = (bit == 0) ? SIGUSR1 : SIGUSR2;
                $(kill(ppid, sig));     //< CRIT stop

                waithandler(ppid);      //< CRIT start (ready)
            }
        }
    } while(it_read != 0);

    $(kill(ppid, SIGIO));       //< throw SIGIO

    exit(EXIT_SUCCESS);         //< throw SIGCHLD
}

void waitbit()
{
    sigset_t set;
    sigemptyset(&set);

    while(!incoming) {
        alarm(ALARM_T);         //< Race condition
        sigsuspend(&set);
        alarm(0);
    }
    incoming = 0;
}

void parent(pid_t cpid)
{
    while(1) {
        waitbit();                  //< CRIT start (incoming, bit, byte)
        if(bit == 8) {
            buf[bufidx++] = byte;
            if(bufidx == BUFSIZE) {
                $(write(STDOUT_FILENO, buf, BUFSIZE));
                bufidx = 0;
            }
            bit = 0;
        }
        $(kill(cpid, SIGIO));       //< CRIT stop
    }
}

